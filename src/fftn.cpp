#include "fftn/fftn.h"

#include <iostream>

namespace fftn
{
namespace utilities
{
std::string getLibVersion()
{
    return FFTN_UTILITIES_VERSION_STRING;
}

} // namespace utilities
} // namespace fftn
