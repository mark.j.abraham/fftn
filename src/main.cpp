#include <fftn/fftn.h>
#include <docopt.h>
#include <iostream>

static const char USAGE[] =
    R"(fftn_exe

    Usage:
        fftn_exe
        fftn_exe (-h | --help)
        fftn_exe --version

    Options:
      -h --help        Show this screen.
      --version        Show version.
)";

int main(int argc, char *argv[])
{
    std::map<std::string, docopt::value> args =
        docopt::docopt(USAGE,
                       {argv + 1, argv + argc},
                       true,   // show help if requested
                       fftn::utilities::getLibVersion()); // version string
    std::cout << "Argument Parsed: " << std::endl;
    for (const auto &item : args)
    {
        std::cout << " " << item.first << ": " << item.second << std::endl;
    }
}
