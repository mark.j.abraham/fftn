#ifndef FFTN_UTILITIES_FFTN_H
#define FFTN_UTILITIES_FFTN_H

#include <string>
#include "fftn/version.h"

namespace fftn
{
namespace utilities
{
std::string getLibVersion();

} // namespace utilities
} // namespace fftn

#endif // FFTN_UTILITIES_FFTN_H
